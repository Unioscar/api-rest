'use strict'
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

const port = process.env.PORT || 3000;

const { response } = require('express');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const app = express();

const URL_WS_Vuelos = "https://localhost:3002/api/vuelo";
const URL_WS_Banco = "https://localhost:4001/api/pago";
const URL_WS_Vehiculos = "https://localhost:3003/api/vehiculo";
const URL_WS_Hoteles = "https://localhost:3001/api/hotel";

const Token = require('./services/token.service');

//Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended : false}))
app.use(express.json())
const https = require('https');
const fs = require('fs');

const cors = require('cors');
// Middlewares
var allowCrossTokenHeader = (req, res, next) => {
res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

const opciones = {
    key : fs.readFileSync('./cert/key.pem'),
    cert : fs.readFileSync('./cert/cert.pem')
}

function auth(req, res, next) {
    if (!req.headers.authorization){ //Mirar si en la cabecera hay un token.
        res.status(400).json({
            result: 'KO',
            mensajes: "No has enviado el token en la cabecera."
        });
        return next();
    }
    const queToken = req.headers.authorization.split(" ")[1]; // token en formato JWT
    Token.decodificaToken(queToken)
    .then(userID => {
        return next();
    })
    .catch(err => {
        res.status(403).json({
            result: 'KO',
            mensajes: "Acceso no autorizado a este servicio."
        });
        return next(new Error("Acceso no autorizado a este servicio."));
    })
}

//Rutas
app.get('/api/:services',(req,res,next) => {
    const services = req.params.services;
    console.log( `se le pasa el servicio ${services}`);
    var queURL;
    if(services == "hotel"){
        queURL = `${URL_WS_Hoteles}/`;
    }
    else if(services == "vuelo"){
        queURL = `${URL_WS_Vuelos}/`;
    }
    else if(services == "vehiculo"){
        queURL = `${URL_WS_Vehiculos}/`;
    }
    else{
        res.status(400).json({
            description : `${servicio} no es un servicio disponible`
        });
    }
    fetch(queURL, {method: 'GET', headers: {'Content-Type': 'application/json'}}).then(resp => 
        resp.json()).then(json => {
            res.json({
                respuesta : json
            });
        }).catch(function(err) {
            res.json({
                description: `Error de conexion con los proveedores`
            });
        });
});

app.get('/api/:services/:id',(req,res,next) => {
    const services = req.params.services;
    const id = req.params.id;
    var queURL;

    if(services == "hotel"){
        queURL = `${URL_WS_Hoteles}/${id}`;
    }
    else if(services == "vuelo"){
        queURL = `${URL_WS_Vuelos}/${id}`;
    }
    else if(services == "vehiculo"){
        queURL = `${URL_WS_Vehiculos}/${id}`;
    }
    else{
        response.status(400).json({
            description : `${servicio} no es un servicio disponible`
        });
    }
    fetch(queURL, {method: 'GET', headers: {'Content-Type': 'application/json'}}).then(resp => 
        resp.json()).then(json => {
            res.json({
                respuesta : json
            });
        }).catch(function(err) {
            res.json({
                description: `Error de conexion con los proveedores`
            });
        });
});

//Crear reserva 
app.put('/api/reservar',async (req,res,next) =>{
    const usuario = req.body.Usuario;
    console.log(req.body.vuelo);
    const vuelo = req.body.vuelo;
    const vehiculo = req.body.vehiculo;
    const hotel = req.body.hotel;
    const tokenUsuario = req.headers.authorization.split(" ")[1];

    let respuestaHotel={}; let compensacionHotel={};
    let respuestaVuelo={}; let compensacionVuelo={};
    let respuestaVehiculo={}; let compensacionVehiculo={};
    let respuestaBanco={}; let compensacionBanco={};

    let mensajeCompHotel = "";
    let mensajeCompVuelo = "";
    let mensajeCompVehiculo = "";
    let mensaje = "";

    let errorConexion = false;
    let errorReserva = false;

    const bodyUsuario = {
        email : req.body.Usuario.email
    }

    //Comprobamos que existe el usuario 
    if(usuario == null){
        res.status(400).json({
            error: "falta el usuario"
        });
    } 
    else {
        //Reservamos un hotel
        if(hotel != null){
            const queURL = `${URL_WS_Hoteles}/${hotel}`;
            respuestaHotel = await fetch(queURL,{method: 'PUT',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    console.log(json);
                    respuestaHotel=Object.assign({},json);
                    return respuestaHotel;
                }).catch(function(err){
                    console.error(err);
                    errorConexion = true;
                    return err;
                });
            //console.log(respuestaHotel);
            if(Object.keys(respuestaHotel).length === 0){
                mensaje = "Error al reservar el hotel, se ha cerrado la transacción";
                errorReserva = true;
            }
            if(respuestaHotel.description == "Este hotel ya ha sido reservado"){
                mensaje = "Error al reservar el hotel,se ha cerrado la transacción";
                errorReserva = true;
            }
            if(errorConexion){
                mensaje = "no se ha podido establecer la conexión, se ha cerrado la transacción";
            }
        }
        //Reservamos un vehiculo, en el caso de haber reservado un hotel antes deberemos de comprobar que no se ha caido la conexion y se ha efectuado correctamente la reserva
        if(vehiculo != null && !errorConexion && !errorReserva){
            const queURL = `${URL_WS_Vehiculos}/${vehiculo}`;
            respuestaVehiculo = await fetch(queURL,{method: 'PUT',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    respuestaVehiculo=Object.assign({},json);
                    return respuestaVehiculo;
                }).catch(function(err){
                    console.error(err);
                    errorConexion = true;
                    return err;
                });
                
            if(Object.keys(respuestaVehiculo).length === 0){
                mensaje = "Error al reservar el vehiculo, se ha cerrado la transacción";
                errorReserva = true;
            }
            if(respuestaVehiculo.description == "Este vehiculo ya ha sido reservado"){
                mensaje = "Error al reservar el vehiculo,se ha cerrado la transacción";
                errorReserva = true;
            }
            if(errorConexion){
                mensaje = "no se ha podido establecer la conexión, se ha cerrado la transacción";
            }
            //Comprobamos que no haya una reserva previa de un hotel, si es asi y la conexion se ha caido o ne se ha completado la reserva cancelamos la reserva del HOTEL
            if(hotel!=null && (errorReserva || errorConexion)){
                const queURL = `${URL_WS_Hoteles}/${hotel}/cancelar`;
                compensacionHotel = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionHotel=Object.assign({},json);
                    return compensacionHotel;
                })
                mensajeCompHotel = "Se ha anulado el hotel que se habia reservado";
                respuestaHotel = {};
            }
        }
        //Reservamos un vuelo
        if(vuelo != null && !errorConexion && !errorReserva){
            const queURL = `${URL_WS_Vuelos}/${vuelo}`;
            respuestaVuelo = await fetch(queURL,{method: 'PUT',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    respuestaVuelo=Object.assign({},json);
                    return respuestaVuelo;
                }).catch(function(err){
                    console.error(err);
                    errorConexion = true;
                    return err;
                });
                //console.log(Object.keys(respuestaVuelo).length);
            if(Object.keys(respuestaVuelo).length === 0){
                mensaje = "Error al reservar el vuelo, se ha cerrado la transacción";
                errorReserva = true;
            }
            if(respuestaVuelo.description == "Este vuelo ya ha sido reservado"){
                mensaje = "Error al reservar el vuelo,se ha cerrado la transacción";
                errorReserva = true;
            }
            if(errorConexion){
                mensaje = "no se ha podido establecer la conexión, se ha cerrado la transacción";
            }
            //Comprobamos que no haya una reserva previa de un hotel o de un vehiculo, si es asi y la conexion se ha caido o ne se ha completado la reserva cancelamos la reserva del HOTEL y del vehiculo
            if(hotel!=null && (errorReserva || errorConexion)){
                const queURL = `${URL_WS_Hoteles}/${hotel}/cancelar`;
                compensacionHotel = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionHotel=Object.assign({},json);
                    return compensacionHotel;
                })
                mensajeCompHotel = "Se ha anulado el hotel que se habia reservado";
                respuestaHotel = {};
            }
            if(vehiculo!=null && (errorReserva || errorConexion)){
                const queURL = `${URL_WS_Vehiculos}/${vehiculo}/cancelar`;
                compensacionVehiculo = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionVehiculo=Object.assign({},json);
                    return compensacionVehiculo;
                })
                mensajeCompVehiculo = "Se ha anulado el vehiculo que se habia reservado";
                respuestaVehiculo = {};
            }
        }
        console.log(vuelo);
        //Vamos a comprobar que se ha hecho alguna reserva y pasamos con el pago
        if((vehiculo != null || hotel != null || vuelo!=null) && !errorConexion && !errorReserva){
            //console.log(respuestaVuelo);
            const queURL = `${URL_WS_Banco}`;
            respuestaBanco = await fetch(queURL,{method: 'GET',headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    respuestaBanco=Object.assign({},json);
                    return respuestaBanco;
                }).catch(function(err){
                    console.error(err);
                    errorConexion = true;
                    return err;
                });
            if(Object.keys(respuestaBanco).length === 0){
                mensaje = "Error al reservar el banco, se ha cerrado la transacción";
                errorReserva = true;
            }
            //console.log(respuestaBanco.resultado);
            if(respuestaBanco.resultado == false){
                mensaje = "no se ha podido proceder con el pago, se ha cerrado la transacción";
                errorConexion = true;
            }
            //Comprobamos que no haya una reserva previa de un hotel o de un vehiculo, si es asi y la conexion se ha caido o ne se ha completado la reserva cancelamos la reserva del HOTEL y del vehiculo
            if(hotel!=null && (errorReserva || errorConexion)){
                const queURL = `${URL_WS_Hoteles}/${hotel}/cancelar`;
                compensacionHotel = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionHotel=Object.assign({},json);
                    return compensacionHotel;
                })
                mensajeCompHotel = "Se ha anulado el hotel que se habia reservado";
                respuestaHotel = {};
            }
            if(vehiculo!=null && (errorReserva || errorConexion)){
                const queURL = `${URL_WS_Vehiculos}/${vehiculo}/cancelar`;
                compensacionVehiculo = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionVehiculo=Object.assign({},json);
                    return compensacionVehiculo;
                })
                mensajeCompVehiculo = "Se ha anulado el vehiculo que se habia reservado";
                respuestaVehiculo = {};
            }
            if(vuelo!=null && (errorReserva || errorConexion)){
                const queURL = `${URL_WS_Vuelos}/${vuelo}/cancelar`;
                compensacionVuelo = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionVuelo=Object.assign({},json);
                    return compensacionVuelo;
                })
                mensajeCompVuelo = "Se ha anulado el vuelo que se habia reservado";
                respuestaVuelo = {};
            }
        }
        var estado = 'OK';
        if(Object.keys(respuestaHotel).length === 0 || Object.keys(respuestaVuelo).length === 0 || Object.keys(respuestaVehiculo).length === 0){
            estado = 'KO';
        }

        res.json({
            Estado : estado,
            respuestaHotel : respuestaHotel,
            respuestaVuelo : respuestaVuelo,
            respuestaVehiculo : respuestaVehiculo,
            respuestaBanco : respuestaBanco,
            Error_conexion : errorConexion,
            Error_reserva : errorReserva,
            mensaje_compensacion_hotel : mensajeCompHotel,
            mensaje_compensacion_vuelo: mensajeCompVuelo,
            mensaje_compensacion_vehiculo : mensajeCompVehiculo,
            Mensaje : mensaje
        })
        
    }
});
//Cancelar reserva
app.delete('/api/cancelar',auth,async(req,res,next) => {
    const usuario = req.body.Usuario;
    const vuelo = req.body.vuelo;
    const vehiculo = req.body.vehiculo;
    const hotel = req.body.hotel;
    console.log(usuario);
    const tokenUsuario = req.headers.authorization.split(" ")[1];

    let respuestaHotel = {};    let compensacionHotel = {};
    let respuestaVuelo = {};    let compensacionVuelo = {};
    let respuestaVehiculo = {}; let compensacionVehiculo = {};
    let respuestaBanco = {};    let compensacionBanco = {};

    let mensajeCompHotel = "";
    let mensajeCompVuelo = "";
    let mensajeCompVehiculo = "";
    let mensaje = "";

    let errorConexion = false;
    let errorReserva = false;

    const bodyUsuario = {
        email : req.body.Usuario.email
    }

    //Comprobamos que existe el usuario
    if(usuario == null){
        res.status(400).json({
            error: "falta el usuario"
        });
    }
    else{
        //Cancelamos un hotel
        if(hotel != null){
            const queURL = `${URL_WS_Hoteles}/${hotel}/cancelar`;
            respuestaHotel = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    console.log(json);
                    respuestaHotel=Object.assign({},json);
                    return respuestaHotel;
                }).catch(function(err){
                    console.error(err);
                    errorConexion = true;
                    return err;
                });
            
            if(Object.keys(respuestaHotel).length === 0){
                mensaje = "Error al cancelar la reserva del hotel, se ha cerrado la transacción";
                errorReserva = true;
            }
            if(respuestaHotel.description == "este elemento no esta reservado, no se puede cancelar la reserva"){
                mensaje = "Error al cancelar la reserva el hotel no estaba reservado,se ha cerrado la transacción";
                errorReserva = true;
            }
            if(errorConexion){
                mensaje = "no se ha podido establecer la conexión, se ha cerrado la transacción";
            }
        }
        //Cancelamos el vehiculo
        if(vehiculo != null && !errorConexion && !errorReserva){
            const queURL = `${URL_WS_Vehiculos}/${vehiculo}/cancelar`;
            respuestaVehiculo = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    respuestaVehiculo=Object.assign({},json);
                    return respuestaVehiculo;
                }).catch(function(err){
                    console.error(err);
                    errorConexion = true;
                    return err;
                });
            
            if(Object.keys(respuestaVehiculo).length === 0){
                mensaje = "Error al cancelar la reservar del vehiculo, se ha cerrado la transacción";
                errorReserva = true;
            }
            if(respuestaVehiculo.description == "este elemento no esta reservado, no se puede cancelar la reserva"){
                mensaje = "Error al cancelar la reserva el vehiculo no estaba reservado,se ha cerrado la transacción";
                errorReserva = true;
            }
            if(errorConexion){
                mensaje = "no se ha podido establecer la conexión, se ha cerrado la transacción";
            }
            if(hotel != null &&(errorReserva || errorConexion)){
                const queURL = `${URL_WS_Hoteles}/${hotel}`;
                compensacionHotel = await fetch(queURL,{method: 'PUT',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionHotel=Object.assign({},json);
                    return compensacionHotel;
                })
                mensajeCompHotel = "Se ha anulado el hotel que se habia cancelado";
                respuestaHotel = {};
            }
        }

    }
    //Cancelamos el vuelo
    if(vuelo != null && !errorConexion && !errorReserva){
        const queURL = `${URL_WS_Vuelos}/${vuelo}/cancelar`;
            respuestaVuelo = await fetch(queURL,{method: 'DELETE',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    respuestaVuelo=Object.assign({},json);
                    return respuestaVuelo;
                }).catch(function(err){
                    console.error(err);
                    errorConexion = true;
                    return err;
                });
                //console.log(Object.keys(respuestaVuelo).length);
            if(Object.keys(respuestaVuelo).length === 0){
                mensaje = "Error al cancelar la reserva del vuelo, se ha cerrado la transacción";
                errorReserva = true;
            }
            if(respuestaVuelo.description == "este elemento no esta reservado, no se puede cancelar la reserva"){
                mensaje = "Error al cancelar la reserva el vuelo no estaba reservado,se ha cerrado la transacción";
                errorReserva = true;
            }
            if(errorConexion){
                mensaje = "no se ha podido establecer la conexión, se ha cerrado la transacción";
            }
            if(hotel!=null && (errorReserva || errorConexion)){
                const queURL = `${URL_WS_Hoteles}/${hotel}`;
                compensacionHotel = await fetch(queURL,{method: 'PUT',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionHotel=Object.assign({},json);
                    return compensacionHotel;
                })
                mensajeCompHotel = "Se ha anulado el hotel que se habia cancelado";
                respuestaHotel = {};
            }
            if(vehiculo!=null && (errorReserva || errorConexion)){
                const queURL = `${URL_WS_Vehiculos}/${vehiculo}`;
                compensacionVehiculo = await fetch(queURL,{method: 'PUT',body: JSON.stringify(bodyUsuario),headers: {'Content-Type': 'application/json','Authorization' : `Bearer ${tokenUsuario}`}}).then(resp => 
                resp.json()).then(json => {
                    compensacionVehiculo=Object.assign({},json);
                    return compensacionVehiculo;
                })
                mensajeCompVehiculo = "Se ha anulado el vehiculo que se habia cancelado";
                respuestaVehiculo = {};
            }
    }

    res.json({
        respuestaHotel : respuestaHotel.description,
        respuestaVuelo : respuestaVuelo.description,
        respuestaVehiculo : respuestaVehiculo.description,
        Error_conexion : errorConexion,
        Error_reserva : errorReserva,
        mensaje_compensacion_hotel : mensajeCompHotel,
        mensaje_compensacion_vuelo: mensajeCompVuelo,
        mensaje_compensacion_vehiculo : mensajeCompVehiculo,
        Mensaje : mensaje
    })

})

https.createServer(opciones, app).listen(port, () => {
    console.log(`API Transacciones ejecutándose en https://localhost:${port}/api/`);
});