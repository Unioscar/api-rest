'use strict'
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

const port = process.env.PORT || 3001;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();

var db = mongojs("mongodb+srv://Unioscar:labaya40@cluster0.obfin.mongodb.net/Hoteles?retryWrites=true&w=majority"); // Enlazando con la base de datos MongoAtlas
var id = mongojs.ObjectID; // Función para convertir un id textual en un objeto de mongojs
const hoteles = db.collection("Hotel");
const https = require('https');
const fs = require('fs');

const opciones = {
    key : fs.readFileSync('./cert/key.pem'),
    cert : fs.readFileSync('./cert/cert.pem')
}

const Token = require('./services/token.service');

const cors = require('cors');
// Middlewares
var allowCrossTokenHeader = (req, res, next) => {
res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

//Middleware

app.use(logger('dev'));
app.use(express.urlencoded({extended : false}))
app.use(express.json())

//Autorización
function auth(req, res, next) {
    if (!req.headers.authorization){ //Mirar si en la cabecera hay un token.
        res.status(400).json({
            result: 'KO',
            mensajes: "No has enviado el token en la cabecera."
        });
        return next();
    }
    const queToken = req.headers.authorization.split(" ")[1]; // token en formato JWT
    Token.decodificaToken(queToken)
    .then(userID => {
        return next();
    })
    .catch(err => {
        res.status(403).json({
            result: 'KO',
            mensajes: "Acceso no autorizado a este servicio."
        });
        return next(new Error("Acceso no autorizado a este servicio."));
    })
}
// Rutas y Controladores

//Lista todos los hoteles de la base de datos
app.get('/api/hotel',(req, res, next) => {
    console.log('GET /api/hotel');

    hoteles.find((err,hotel) => {
        if(err) return next(err);
        res.json(hotel);
    });
});

//Lista el hotel cuyo nombre corresponda con el pasado por parametro
app.get('/api/hotel/:id',(req, res, next) => {

    hoteles.findOne({_id: id(req.params.id)},(err, elemento) => {
        if (err) return next (err);
        res.json(elemento);
    });
});

//Añade un hotel
app.post('/api/hotel',(req, res, next) => {
    const elemento = req.body;

    if(!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    } 
    else {
        hoteles.save(elemento, (err,hotelGuardado) => {
            if(err) return next(err);
            res.json({
                result : 'OK',
                hotel : hotelGuardado
            });
        });
    }
});

//Reserva un hotel
app.put('/api/hotel/:id',(req, res, next) => {
    let elementoId = req.params.id;
    hoteles.findOne({_id: id(elementoId)},(err, elemento) => {
        //console.log("Hola");
        if (err) return next (err);
        if(!elemento.reservado) {

            var reserva = {
                "reservado":true
            };
            hoteles.update({_id: id(elementoId)},
            {$set: reserva},{safe: true, multi: false},(err,elementoModif) => {
                //console.log("Vamos a reservar el hotel");
                if (err) return next(err);
                res.status(200).json({
                    description: 'Reservado'
                });
            });
        }
        else{
            res.status(400).json({
                description: 'Este hotel ya ha sido reservado'
            });
        }
    });
});

app.delete('/api/hotel/:id/cancelar',(req,res,next) => {
    let elementoId = req.params.id;

    hoteles.findOne({_id: id(elementoId)}, (err, elemento) => {
        if (err) return next (err);
        if(elemento.reservado) {

            var reserva = {
                "reservado" : false
            };

            hoteles.update({_id: id(elementoId)},
            {$set: reserva},{safe: true, multi: false},(err,elementoModif) => {
                if (err) return next(err);
                res.status(200).json({
                    description : `se ha cancelado el hotel con id :${elementoId}`,
                    elemento : elementoModif
                });
            });
        }
        else {
            res.status(400).json({
                description : "este elemento no esta reservado, no se puede cancelar la reserva"
            });
        }
    });
});

//Iniciamos la aplicación

https.createServer(opciones, app).listen(port, () => {
    console.log(`API HOTELES ejecutándose en https://localhost:${port}/api/hotel`);
});