'use strict'
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

const port = process.env.PORT || 3004;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const moment = require('moment');
const helmet = require('helmet');
const Password = require('./services/pass.service');
const Token = require('./services/token.service');

const app = express();
var db = mongojs("mongodb+srv://Unioscar:labaya40@cluster0.obfin.mongodb.net/Usuarios?retryWrites=true&w=majority");
var id = mongojs.ObjectID;
const usuarios = db.collection("Usuario");
const https = require('https');
const fs = require('fs');

const opciones = {
    key : fs.readFileSync('./cert/key.pem'),
    cert : fs.readFileSync('./cert/cert.pem')
}

const cors = require('cors');
// Middlewares
var allowCrossTokenHeader = (req, res, next) => {
res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

//middleware

app.use(logger('dev'));
app.use(express.urlencoded({extended : false}))
app.use(express.json())



// rutas

app.get('/api/usuario',(req,res,next) => {
    usuarios.find((err,elemento) => {
        if(err) return next(err);
        res.json(elemento);
    });
});

app.get('/api/usuario/:id', (req,res,next) => {
    usuarios.findOne({_id: id(req.params.id)},(err, elemento) => {
        if (err) return next (err);
        console.log(elemento);
        res.json(elemento);
    });
});

app.post('/api/registrar',(req,res,next) => {
    const elemento = req.body;

    if(!elemento.nick || !elemento.email || !elemento.passw){
        res.status(400).json({
            error: 'Bad data', 
            description: 'No se han rellenado todos los campos obligatorios'
        });
    }
    else{
        console.log(elemento.email);
        usuarios.findOne({email: elemento.email}, (err, user) => {
            console.log(err);
            if(err) return next(err);
            if(user == null){
                console.log(user);
                Password.encriptaPassword(elemento.passw).then(hash => {
                    elemento.passw = hash;
                    usuarios.save(elemento, (err, elementoNuevo) => {
                        if(err) return next(err);
                        res.status(200).json({
                            descripcion : "Usuario guardado"
                        });
                    });
                });
            }
            else{
                res.status(400).json({
                    error: 'Error',
                    descripcion: "El usuario ya existe en nuestra base de datos"
                });
            }
        });
    }
});

app.put('/api/login',(req,res,next) => {
    const elemento = req.body;
    /*
    res.status(200).json({
        elemento : elemento
    });
    */
    usuarios.findOne({email: elemento.email}, (err,user) => {
        if(user){
            console.log("Me llamaban alex el payaso de PPSS");
            Password.comparaPassword(elemento.passw, user.passw).then( compara => {
                if(compara){
                    const token = Token.creaToken(user);//Se crea el token del usuario
                    const temp = {
                        lastLoginDate: moment(Date.now()).format('LLLL'),
                        isActive: true
                    }

                    usuarios.update({_id: id(user._id)}, {$set: temp}, {safe: true, multi: false}, (err, resultado) => {
                        if(err) return next(err);
                        res.status(200).json({
                            descripcion: "Se ha iniciado sesion correctamente",
                            token : token
                        });
                    });
                }
                else{
                    res.status(400).json({
                        error: 'Error',
                        descripcion: "La contraseña no coincide"
                    });
                }
            });
        }
        else{
            res.status(400).json({
                error:'Error',
                descripcion: "El usuario no existe"
            });
        }
    });
});

//Iniciamos la aplicación

https.createServer(opciones, app).listen(port, () => {
    console.log(`API USUARIOS ejecutándose en https://localhost:${port}/api/usuarios`);
});