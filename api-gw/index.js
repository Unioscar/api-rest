'use strict'
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

const port = process.env.PORT || 3100;

const { response } = require('express');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');

const Token = require('./services/token.service');

const URL_WS_Transactions = "https://172.20.42.6:3000/api";
const URL_WS_User = "https://localhost:3004/api";
const https = require('https');
const app = express();
const fs = require('fs');

const opciones = {
    key : fs.readFileSync('./cert/key.pem'),
    cert : fs.readFileSync('./cert/cert.pem')
}

const cors = require('cors');
// Middlewares
var allowCrossTokenHeader = (req, res, next) => {
res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

//middleware

app.use(logger('dev'));
app.use(express.urlencoded({extended : false}))
app.use(express.json())

//Autorizacion
function auth(req, res, next) {
    console.log(req.headers.authorization);
    if (!req.headers.authorization){ //Mirar si en la cabecera hay un token.
        res.status(400).json({
            result: 'KO',
            mensajes: "No has enviado el token en la cabecera."
        });
        return next();
    }
    const queToken = req.headers.authorization.split(" ")[1]; // token en formato JWT
    console.log(queToken);
    Token.decodificaToken(queToken)
    .then(userID => {
        return next();
    })
    .catch(err => {
        res.status(403).json({
            result: 'KO',
            mensajes: "Acceso no autorizado a este servicio."
        });
        return next(new Error("Acceso no autorizado a este servicio."));
    })
}

// rutas

app.post('/api/registrar',(req,res,next) => {
    const consulta = `${URL_WS_User}/registrar`;

    //cliente de la api-usuario
    fetch(consulta, {method: 'POST',body: JSON.stringify(req.body),headers: {'Content-Type' : 'application/json'}}).then( resp => 
    resp.json())
    .then(json => { 
        res.json({
            result: 'OK',
            respuesta : json
        });
    });
});

app.post('/api/login',(req,res,next) => {
    const consulta = `${URL_WS_User}/login`;
    
    fetch(consulta, {method: 'PUT',body: JSON.stringify(req.body),headers: {'Content-Type' : 'application/json'}}).then( resp => 
    resp.json())
    .then(json => { 
        res.json({
            result: 'OK',
            respuesta : json
        });
    }).catch(function(err) {
        err.json({
            err: `Error de conexion con los proveedores`
        });
    });
});



app.get('/api/:proveedor', (req,res,next) => {
    
    const proveedor = req.params.proveedor;
    console.log(proveedor);
    var queURL;
    if(proveedor == "hotel"){
        queURL = `${URL_WS_Transactions}/${proveedor}`;
    }
    else if(proveedor == "vuelo"){
        queURL = `${URL_WS_Transactions}/${proveedor}`;    
    }
    else if(proveedor == "vehiculo"){
        queURL = `${URL_WS_Transactions}/${proveedor}`;    
    }
    else{
        res.status(400).json({
            description : `${proveedor} no es un servicio disponible`
        });
    }
    fetch(queURL, {method: 'GET', headers: {'Content-Type': 'application/json'}}).then(resp => 
        resp.json()).then(json => {
            res.json({
                respuesta : json

            });
        }).catch(function(err) {
            res.json({
                description: `Error de conexion con los proveedores desde Transacciones`
            });
        });
});

app.get('/api/:proveedor/:id', (req,res,next) => {
    
    const proveedor = req.params.proveedor;
    const proveedorID = req.params.id;
    var queURL;
    if(proveedor == "hotel"){
        queURL = `${URL_WS_Transactions}/${proveedor}/${proveedorID}`;
    }
    else if(proveedor == "vuelo"){
        queURL = `${URL_WS_Transactions}/${proveedor}/${proveedorID}`;    
    }
    else if(proveedor == "vehiculo"){
        queURL = `${URL_WS_Transactions}/${proveedor}/${proveedorID}`;    
    }
    else{
        res.status(400).json({
            description : `${proveedor} no es un servicio disponible`
        });
    }
    fetch(queURL, {method: 'GET', headers: {'Content-Type': 'application/json'}}).then(resp => 
        resp.json()).then(json => {
            res.json({
                respuesta : json

            });
        }).catch(function(err) {
            res.json({
                description: `Error de conexion con los proveedores desde Transacciones`
            });
        });
});


//crear una reserva 
app.put('/api/reservar',auth,async (req,res,next) => {
    const tokenUsuario = req.headers.authorization.split(" ")[1];
    const urlTrans = `${URL_WS_Transactions}/reservar`;
    var respuesta = {};
    var id; 
    id = await Token.decodificaToken(tokenUsuario).then(id => {return id;});
    const urlUsuario = `${URL_WS_User}/usuario/${id}`;

    respuesta = await fetch(urlUsuario, {method: 'GET'}).then(resp => resp.json())
    .then(json => {
        respuesta = Object.assign({}, json.email);
        return respuesta;
    }).catch(function(err) {
        console.error(err);
        return err;
    });

    const reserva = {
        Usuario : {
            email : respuesta.email
        },
        vuelo : req.body.vuelo,
        vehiculo : req.body.vehiculo,
        hotel : req.body.hotel
    }
    fetch(urlTrans, {method: 'PUT',body:JSON.stringify(reserva),headers: {'Content-Type': 'application/json','Authorization': `Bearer ${tokenUsuario}`}})
    .then(resp => resp.json())
    .then(json => {
        res.status(200).json({
            result: json
        });
    }).catch(function(err) {
        console.error(err);
        return err;
    });
});

//Cancelar reserva
app.delete('/api/cancelar',auth,async(req,res,next)=> {
    const tokenUsuario = req.headers.authorization.split(" ")[1];
    const urlTrans = `${URL_WS_Transactions}/cancelar`;
    var respuesta = {};
    var id; 
    id = await Token.decodificaToken(tokenUsuario).then(id => {return id;});
    const urlUsuario = `${URL_WS_User}/usuario/${id}`;

    respuesta = await fetch(urlUsuario, {method: 'GET'}).then(resp => resp.json())
    .then(json => {
        respuesta = Object.assign({}, json.email);
        return respuesta;
    }).catch(function(err) {
        console.error(err);
        return err;
    });
    console.log(respuesta.email);
    const reserva = {
        Usuario : {
            email : respuesta
        },
        vuelo : req.body.vuelo,
        vehiculo : req.body.vehiculo,
        hotel : req.body.hotel
    }
    fetch(urlTrans, {method: 'DELETE',body:JSON.stringify(reserva),headers: {'Content-Type': 'application/json','Authorization': `Bearer ${tokenUsuario}`}})
    .then(resp => resp.json())
    .then(json => {
        res.status(200).json({
            result: json
        });
    }).catch(function(err) {
        console.error(err);
        return err;
    });
});


//Iniciamos la aplicación
https.createServer(opciones, app).listen(port, () => {
    console.log(`API GATEWAY ejecutándose en https://localhost:${port}/api`);
});