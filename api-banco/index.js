'use strict'
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

const port = process.env.PORT || 4001;
const express = require('express');
const logger = require('morgan');
const BodyParser = require("body-parser");
const fecth = require('node-fetch');
const { response } = require('express');
const app = express();
const https = require('https');
const fs = require('fs');

const opciones = {
    key : fs.readFileSync('./cert/key.pem'),
    cert : fs.readFileSync('./cert/cert.pem')
}

const Token = require('./services/token.service');

const cors = require('cors');
// Middlewares
var allowCrossTokenHeader = (req, res, next) => {
res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

//Middleware 

app.use(logger('dev'));
app.use(express.urlencoded({extended : false}));
app.use(express.json());

function auth(req, res, next) {
    if (!req.headers.authorization){ //Mirar si en la cabecera hay un token.
        res.status(400).json({
            result: 'KO',
            mensajes: "No has enviado el token en la cabecera."
        });
        return next();
    }
    const queToken = req.headers.authorization.split(" ")[1]; // token en formato JWT
    Token.decodificaToken(queToken)
    .then(userID => {
        return next();
    })
    .catch(err => {
        res.status(403).json({
            result: 'KO',
            mensajes: "Acceso no autorizado a este servicio."
        });
        return next(new Error("Acceso no autorizado a este servicio."));
    })
}

app.get('/api/pago',auth,(req,res,next) => {
    const rand = Math.random() * (9 - 0);
    var pago = false;
    if(rand >= 0 && rand<8){
        pago = true;
    }
    res.json({
        resultado : pago
    })
});

https.createServer(opciones, app).listen(port, () => {
    console.log(`API BANCO ejecutándose en https://localhost:${port}/api/pago`);
});