'use strict'
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

const port = process.env.PORT || 3002;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();
var db = mongojs("mongodb+srv://Unioscar:labaya40@cluster0.obfin.mongodb.net/Vuelos?retryWrites=true&w=majority");
var id = mongojs.ObjectID;
const vuelos = db.collection("Vuelo");
const https = require('https');
const fs = require('fs');

const opciones = {
    key : fs.readFileSync('./cert/key.pem'),
    cert : fs.readFileSync('./cert/cert.pem')
}

const Token = require('./services/token.service');

const cors = require('cors');
// Middlewares
var allowCrossTokenHeader = (req, res, next) => {
res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

//middleware

app.use(logger('dev'));
app.use(express.urlencoded({extended : false}))
app.use(express.json())

//Autorización
function auth(req, res, next) {
    if (!req.headers.authorization){ //Mirar si en la cabecera hay un token.
        res.status(400).json({
            result: 'KO',
            mensajes: "No has enviado el token en la cabecera."
        });
        return next();
    }
    const queToken = req.headers.authorization.split(" ")[1]; // token en formato JWT
    Token.decodificaToken(queToken)
    .then(userID => {
        return next();
    })
    .catch(err => {
        res.status(403).json({
            result: 'KO',
            mensajes: "Acceso no autorizado a este servicio."
        });
        return next(new Error("Acceso no autorizado a este servicio."));
    })
}

// rutas


app.get('/api/vuelo',(req, res, next) => {
    vuelos.find((err,vuelo) => {
        if(err) return next(err);
        res.json(vuelo);
    });
});

app.get('/api/vuelo/:id',(req, res, next) => {
    vuelos.findOne({_id: id(req.params.id)},(err, elemento) => {
        if (err) return next (err);
        res.json(elemento);
    });
});

app.post('/api/vuelo',(req, res, next) => {
    const elemento = req.body;

    if(!elemento.compañia) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <compañia>'
        });
    } 
    else {
        vuelos.save(elemento, (err,vueloGuardado) => {
            if(err) return next(err);
            res.status(200).json({
                result: 'OK',
                vuelo : vueloGuardado
            });
        });
    }
});
//Reservar vuelos o cancelar vuelos
app.put('/api/vuelo/:id',(req, res, next) => {
    let elementoId = req.params.id;

    vuelos.findOne({_id: id(elementoId)},(err, elemento) => {
        if (err) return next (err);
        if (!elemento.reservado) {

            var reserva={
                "reservado":true 
            };
            vuelos.update({_id: id(elementoId)},
            {$set: reserva},{safe: true, multi: false},(err,elementoModif) => {
                if (err) return next(err);
                res.status(200).json({
                    description: 'Reservado'
                });
            });
        }
        else {
            res.status(400).json({
                description: 'Este vuelo ya ha sido reservado'
            });
        }

    });
});

app.delete('/api/vuelo/:id/cancelar',(req,res,next) => {
    let elementoId = req.params.id;

    vuelos.findOne({_id: id(elementoId)}, (err, elemento) => {
        if (err) return next (err);
        if(elemento.reservado) {

            var reserva = {
                "reservado" : false
            };

            vuelos.update({_id: id(elementoId)},
            {$set: reserva},{safe: true, multi: false},(err,elementoModif) => {
                if (err) return next(err);
                res.status(200).json({
                    description : `se ha cancelado el vuelo con id :${elementoId}`,
                    elemento : elementoModif
                });
            });
        }
        else {
            res.status(400).json({
                description : "este elemento no esta reservado, no se puede cancelar la reserva"
            });
        }
    });
});
//Iniciamos la aplicación

https.createServer(opciones, app).listen(port, () => {
    console.log(`API VUELOS ejecutándose en https://localhost:${port}/api/vuelo`);
});